from selenium import webdriver
import PageObject
import pytest


# Тесты для Table версии сайта DNS
class TestDNS:

    # Стартовые настройки тестов
    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.set_window_size(900, 800)

    # При завершении всех квестов, закрыть драйвер
    def teardown_class(self):
        self.driver.close()

    # При запуске нового теста, перейти на главкную и поставить задержку
    def setup_method(self, method):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.dns-shop.ru/")

    # Тест на соответствие результата поиска к запросу
    def test_search(self):
        # Запуск страницы поиска
        main_page = PageObject.MainPage(self.driver)
        search_query = "Клавиатура"
        search_result = main_page.search(search_query)

        # Считываем названия найденных товаров и сравниваем их с запросом
        for elem in search_result.search_result_title():
            print("Res: " + elem.text)
            assert search_query in elem.text

    # Тест на правильно добавление товаров в сравнение
    def test_comparison(self):
        # Запуск страницы поиска
        main_page = PageObject.MainPage(self.driver)
        search_result = main_page.search("Клавиатура")

        # Сохранение результата поиска
        comparison = []
        for elem in search_result.search_result_title():
            comparison.append(elem.text)

        # Выбор элементов для 'Сравнение'
        search_result.search_result_checkbox()

        # Переход к страницы 'Сравнение'
        page_comparison = main_page.start_comparison()

        # Получение названий таваров на странице 'Сравнение' и проверка на равенство выбраным объектам
        comparison_result = page_comparison.search_product_in_comparison()
        for i in range(2):
            print("Res: " + str(comparison[i]) + " in " + comparison_result[i].text)
            assert comparison[i] in comparison_result[i].text

    # Тест на правильность заполнения корзины
    def test_cart(self):
        # Номер найденого элемента, который нужно добавить в корзину
        product_id = 0

        # Запуск страницы поиска
        main_page = PageObject.MainPage(self.driver)
        search_result = main_page.search("Клавиатура")

        # Сохраняем название первого товара, так как добавим его в корзину
        product = search_result.search_result_title()[product_id].text

        # Добовление товара под номером product_id и открытие корзины
        search_result.add_to_cart(product_id)
        cart_page = main_page.open_cart()

        # Получаем название продукта, нохоящегося в корзине и сравниваем с продуктом, который добовляли
        result_product = cart_page.name_product_in_cart()
        print("Res: " + result_product + " in " + product)
        assert result_product in product
